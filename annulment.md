# Annulment of Agreement

As of March 30th, the agreement has been voided due to Joshua committing the following:

* Lied
* Caused a family member to not want to take care of him due to Joshua having:
  * Stole money
  * Wiped a person's phone data
  * Stole a person's credit card information

Joshua has been returned to the care of his parents on March 30th 2021

# Recommendations

Joshua has been recommended to fix the following behaviours: 需改的行为/思维
* Dishonest behaviour 不诚实的行为
  * Hypocritical 虚伪
  * Image and dignity 形象和自尊心
    * Lack of trustworthiness 缺乏可信度
  * Boastful behaviour 自傲自夸的行为
  * More lies to cover up previous lies 谎话越说越多越大
  * Bigger boasts to beat previous boasts 炫耀越炫越多越大
* Disrespectful and Unruly behaviour 不尊重和不礼貌的行为
  * Aggression 暴力
    * Aggression as a tool to escape responsibility 暴力以逃避责任的道具
  * Disrespect 不尊重
  * Threatful 威胁性
    * "I want to go back home" “我要收拾我的东西回家” x3
* Responsibility/Accountability 为自己的行为而负责任
  * Study 读书是中学生的主要和基本的责任
  * Admission of mistakes 做错东西不承认
    * Immediate admission of mistakes 做错东西要立刻承认
  * "I want to go back home" “我要收拾我的东西回家” x3