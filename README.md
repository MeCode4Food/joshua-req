# Requirements and Agreement

## Joshua

### Academic Ability as of Jan 2021

Maths:
- Clearly still at standard 3/4 level despite being form 2
- Needs a lot of catching up

English:
- Needs him to converse fully
- Needs him to read weekly

Science:
- Unable to understand simple Malay questions

Other subjects:
- Inability to self study
- Unable to understand simple Malay questions/sentences will cause big problems in the future
- Relies too much on other people to explain sentences

### Habits & Behaviour

- Initial behaviour terrible
  - Talk about terrible use of language at home
  - Absolutely no terrible use of language at home ever
- Alcohol
  - Absolutely no alcohol whatsoever before the age of 20
- Friends & Environment
  - Have to let us know
- Studying and disruption
  - Easily distracted while studying
  - When calling, ASK if he is studying before continuing the call
- Other habits:
  - Roughness & General noisiness
    - Walking/running up down the sttairs
  - Phones while eating
  - Prone to eavesdropping or paying attention to things unrelated to him, or conversations within earshot

## Requirements

### Academic

General Attitude/Habits
- Caretakers can help, but Joshua's academic ability is Joshua's responsibility

English
- Do a reading + exercises every Weekend
  - Unless no one is free to do english reading exercises with Joshua
  - https://agendaweb.org/reading/easy-reading-1.html
  - Make sentences from several words from the article
  - Read the same article the next week
- Need to converse fully in English by end of Form 2
  - This means (roughly), for 10 sentences spoken, 8 must be in english

Results
- Get at least **B** in **Maths, English and Science** in PT3
- Not get lower than **C** in all other subjects in PT3

### Attitude

- DO NOT
  - Aggravate any family members
  - Lie
  - Be Rude
  - Drink/Smoke/Gamble/Do drugs
  - Use bad language at home
  - Do illegal activities
  - Be loud
  - Hide information that he thinks is important
  - Stay up late at night
  - be violent to people or things belonging to people
- DO
  - Listen to family members
  - Respect family members
    - Privacy
  - If there are any things that you suspect, let family members know
  - Ask permission
    - Before going out
    - Before taking anything that is not his
  - Pay attention in class

### List of "Last Chance" behaviors

If Joshua does any of the behaviors below, Joshua will return home

- Lie
- React violently to being scolded
- Steal Money

### Failure of meeting requirements

Joshua will return home before PT3 if:
- Any family members do not want to take care due to bad attitude
- Joshua fails to behave in "Failure of meeting requirements"

Joshua will return after PT3 until family members agree to take him in until Form 5

## Pocket Money
- Caretakers will pay a sum of RM3 for each exercise done:
  - Read an English article from the newspaper:
    - Read out aloud the English article from a newspaper of choice
    - Answer a few (3 or so) questions about the article
    - Joshua will be provided 5 English words from the article, and he must write down the meaning and a sample sentences
    - The sentence must be grammatically correct and accurate to reflect the meaning of the word given

## Notes

- Family members can change requirements as required
- Family members can ask questions about his friends, his phones and he must answer

## Legality
- This is not a legally binding document of any kind